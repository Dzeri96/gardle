#!/bin/bash

languages=(en)

for lang in $languages; do
		mkdir $lang
		for file in de/*; do
		  filebase=`basename $file`
		  node translate-json AIzaSyCLUgVw04_TKFH-1hZgEf11TRHAVX3cp8k $file $lang "$lang/${filebase}"; 
		done
done
